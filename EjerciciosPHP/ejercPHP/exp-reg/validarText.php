
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
  <title>Validación de entrada de texto 1 (Formulario). Expresiones regulares.
  Ejercicios. PHP. Bartolomé Sintes Marco</title>
  <meta name="generator" content="amaya 9.3, see http://www.w3.org/Amaya/" />
  <link href="mclibre_php_soluciones.css" rel="stylesheet" type="text/css"
  title="Color" />
</head>

<body>


<form method="get">
  <h1>Comprobacion de datos</h1>
  <fieldset>
    <legend>Formulario</legend>
    <p>Escriba cualquier cosa en el campo siguiente excepto codigo html
    (&lt;...&gt;) para analizarlo con distintas funciones de PHP:</p>

    <p><strong>Dato:</strong> <input type="text" name="dato" /></p>

    <p class="der">
    <input type="submit" value="Enviar" /> 
    <input type="reset" value="Borrar" name="Reset" /></p>
  </fieldset>
</form>

<?php

if(isset($_GET['dato'])){
	$texto = $_GET['dato'];
	//limpieza del dato, quitar espacios en blanco
	$texto = trim($_GET['dato']);
	//quitar etiquetas html
	$texto = strip_tags(trim($_GET['dato']));
	if(is_numeric($texto)){
			echo '<b>el campo es numerico</b><br>';
			if(is_int(filter_var($texto, FILTER_VALIDATE_INT))){
				echo '<b>el campo es un int</b><br>';
			}
			if(is_float(filter_var($texto, FILTER_VALIDATE_FLOAT))){
				echo '<b>el campo es un float</b><br>';
			}
		}
	else{
		if(is_string($texto)){
			echo '<b>el campo es string</b><br>';
			if(filter_var($texto, FILTER_VALIDATE_EMAIL)){
				echo $texto, ' es un email<br>';
			}
		}
	}
}
else{
	echo '<b>el campo esta vacio!</b>';
}


?>

</body>
</html>
