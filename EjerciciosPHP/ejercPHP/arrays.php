<?php

$lista = array();

$lista[] = "DAW0";
$lista[] = "DAW1";
$lista[] = "DAW2";
$lista[] = "DAW3";
$lista[] = "DAW4";

print_r($lista);
echo '<hr>';

unset ($lista[2]); //eliminar elemento num 2

echo 'se ha eliminado elemento 2';
echo '<hr>';

print_r($lista);
echo '<hr>';

//recorrer lista
for ($i=0; $i < count($lista); $i++){
	echo $i, '-->', $lista[$i]. '<br>';
}
echo '<hr>';
//otra forma de recorrer
foreach($lista as $i => $curso){
	echo $i, ' -> ', $curso, '<br>';
}
echo '<hr>';

//arays clave valor asociativos
$colega = array(pepe => amigo, maria => prima);
echo 'array clave valor:<br>';

foreach($colega as $i => $tipo){
	echo $i, ' -> ', $tipo, '<br>';
}
echo '<hr>';

?>
