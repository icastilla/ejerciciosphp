
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
  <title>Contador de días (Formulario). Repaso 1. 
  Ejercicios. PHP. Bartolomé Sintes Marco</title>
  <meta name="generator" content="amaya 8.7.1, see http://www.w3.org/Amaya/" />
  <link href="mclibre_php_soluciones.css" rel="stylesheet" type="text/css"
  title="Color" />
</head>

<body>
<h1>Contador de días (Formulario)</h1>

<form method="get">
  <fieldset>
    <legend>Formulario</legend>
    <p>Escriba un número de semanas (0 &lt; semanas &le; 20) y mostraré un
    calendario.</p>

    <table cellspacing="5" class="borde">
      <tbody>
        <tr>
          <td><strong>Número de semanas:</strong></td>
          <td><input type="text" name="numero" size="4" maxlength="2" /></td>
        </tr>
      </tbody>
    </table>

    <p class="der">
    <input type="submit" value="Enviar" /> 
    <input type="reset" value="Borrar" name="Reset" /></p>
  </fieldset>
</form>

<?php

if(isset($_GET['numero'])){
	include("contador_dias.php");
	include("dias.php");
	$numero = $_GET['numero'];
	if ($numero>0 && $numero <=20){
		muestraTabla($numero);
	}
	else{
		echo "<strong>Número no válido</strong>";
	}
}
if(isset($_GET['dias'])){
	include("contador_dias.php");
	$datos = $_GET['dias'];
	muestra_dias($datos);
}
?>


</body>
</html>