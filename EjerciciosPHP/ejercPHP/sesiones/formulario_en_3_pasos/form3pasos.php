<?php
	session_start();
	
	
	
?>

<?php
	if (isset($_GET['nombre'])){
		$nombre = $_GET['nombre'];
		$_SESSION['nombre'] = $nombre;
	}

print_r($_SESSION);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
  <title>Formulario en tres pasos (Formulario 1). Sesiones. 
  Ejercicios. PHP. Bartolomé Sintes Marco</title>
  <meta name="generator" content="amaya 8.7.1, see http://www.w3.org/Amaya/" />
  <link href="mclibre_php_soluciones.css" rel="stylesheet" type="text/css"
  title="Color" />
</head>

<body>
<h1>Formulario en tres pasos (Formulario 1)</h1>

<form action="" method="get">
  <fieldset>
    <legend>Formulario</legend>
    <p>Escriba su nombre:</p>

    <table cellspacing="5" class="borde">
      <tbody>
        <tr>
          <td><strong>Nombre:</strong></td>
          <td><input type="text" name="nombre" size="20" maxlength="20" 
          	<?php if (isset($_SESSION['nombre']))
				echo "value=".'"'.$_SESSION['nombre'].'"';
				?>
          	/></td>
        </tr>
      </tbody>
    </table>

    <p class="der">
    <input type="submit" value="Seguir" /> 
    <input type="reset" value="Borrar" name="Reset" /></p>
  </fieldset>
</form>





</body>
</html>
