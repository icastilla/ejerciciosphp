<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <title>Validacion de formulario (Formulario). Validación. Ejercicios. PHP. Bartolomé Sintes Marco</title>
  <link href="mclibre_php_soluciones.css" rel="stylesheet" type="text/css"
  title="Color" />
</head>

<body>
<h1>Validación de formulario (Formulario)</h1>
<p>Escriba los datos siguientes:</p>
<form method="get">
  <table>
    <tbody>
      <tr>
        <td>Nombre trabajador:</td>
        <td><input type="text" name="nombre" size="40"
          maxlength="40" value="" /></td>
      </tr>
      <tr>
        <td>Edad:</td>
        <td><input type="number" name="edad" size="2"
          maxlength="9" value="" /></td>
      </tr>
      <tr>
        <td>Email:</td>
        <td><input type="email" name="email" size="40"
          maxlength="40" value="" /></td>
      </tr>
      <tr>
        <td>Sueldo:</td>
        <td><input type="number" name="sueldo" size="40"
          maxlength="40" value="" /></td>
      </tr>
      <tr>
        <td>Retencion IRPF:</td>
        <td><input type="number" name="irpf" size="40"
          maxlength="40" value="" /></td>
      </tr>
    </tbody>
  </table>
  <p class="der"><input type="submit" name="enviar" value="Enviar" /></p>
</form>

<?php

if(isset($_GET['nombre'])){
	$nombre = strip_tags(htmlspecialchars(trim($_GET['nombre'])));
}
if(isset($_GET['edad'])){
	$edad = strip_tags(trim($_GET['edad']));
}
if(isset($_GET['email'])){
	$email = strip_tags(trim($_GET['email']));
}
if(isset($_GET['sueldo'])){
	$sueldo = strip_tags(trim($_GET['sueldo']));
}
if(isset($_GET['irpf'])){
	$irpf = strip_tags(trim($_GET['irpf']));
}
	
	
 
?>

</body>
</html>