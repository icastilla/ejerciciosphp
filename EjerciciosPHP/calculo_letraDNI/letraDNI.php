<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
  <title>Calculadora de letra del DNI (Formulario). Repaso 1. 
  Ejercicios. PHP. Bartolomé Sintes Marco</title>
  <meta name="generator" content="amaya 8.7.1, see http://www.w3.org/Amaya/" />
  <link href="mclibre_php_soluciones.css" rel="stylesheet" type="text/css"
  title="Color" />
</head>

<body>
<h1>Calculadora de letra del DNI (Formulario)</h1>

<form method="get">
  <fieldset>
    <legend>Formulario</legend>
    <p>Escriba el número del DNI y calcularé la letra correspondiente.</p>

    <table cellspacing="5" class="borde">
      <tbody>
        <tr>
          <td><strong>Número:</strong></td>
          <td><input type="text" name="dni" size="10" maxlength="10" /></td>
        </tr>
      </tbody>
    </table>

    <p class="der">
    <input type="submit" value="Calcular" /> 
    <input type="reset" value="Borrar" name="Reset" /></p>
  </fieldset>
</form>

<?php

	if (isset($_GET['dni'])){
		include("funcionLetraDNI.php");
		
		$dni = $_GET['dni'];
		
		printf ("<p>Letra del DNI $dni: %s </p>", letra($dni));
	}
?>

</body>
</html>