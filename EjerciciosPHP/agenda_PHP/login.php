<?php
include_once 'lib/config.php';
include_once 'lib/funciones.php';

// iniciamos sesion
session_start();


// variable para errores
$errores = array();

// si viene por post la info del formulario login.html
if(isset($_POST['user'], $_POST['passwd'])){
	
	//validar, clean es una funcion dentro de funciones.php
	$user = clean($_POSt['user']);
	$passwd = clean($POST[passwd]);
	
	if (validar($user, $passwd)){
		$_SESSION['user'] = $user;
		header('Location: index.php');
	}
	$errores [] = "Usuario o password no valido";
	
	//comprobarlas (siempre despues de validarlas)
}

//separar presentacion
$template = $twig->loadTemplate('login.html');
echo $template->render(array('titulo'=>'login',
							'errores' =>$errores,
							));

?>