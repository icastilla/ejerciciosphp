<?php
// Datos de configuracion

// Datos del administrador
$root = 'root';
$passwd_admin = 'root';

// Configuracion twig (motor de plantillas)
require_once realpath(dirname(__FILE__) . '/../vendor/twig/twig/lib/Twig/Autoloader.php'); //crea ruta absoluta para el cargador de twig

Twig_Autoloader::register(); 

$loader = new Twig_Loader_Filesystem(realpath(dirname(__FILE__) . '/../vistas')); //instancia del cargador, indicamos donde estan las plantillas

$twig = new Twig_Environment($loader); //esto es lo que vamos a usar como motor de plantillas a partir de ahora





?>