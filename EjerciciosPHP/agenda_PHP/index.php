<?php
include 'lib/config.php'; //para acceder a twig

// gestion de sesion para autentificacion
session_start();

//$_SESSION['user'] = 'yo'; //con esta frase entra con una sesion y no redirige a login

//si el usuario no esta registrado en sesion redirige a login
if (!isset($_SESSION['user'])){
	header('Location: login.php');
}


$template = $twig->loadTemplate('inicio.html'); 

// escribir logica del programa
$amigos = array('Luis', 'Maria', 'Pepe',);

$datos = array('titulo'=>'Primera página',
				'nombre' =>'Idoia',
				'amigos' => $amigos,
				);

echo $template->render($datos); //le pasamos datos mediante un array clave-valor


?>